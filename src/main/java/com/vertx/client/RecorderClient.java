package com.vertx.client;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;

import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.AsyncResultHandler;
import org.vertx.java.core.Handler;
import org.vertx.java.core.Vertx;
import org.vertx.java.core.VertxFactory;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.net.NetSocket;

public class RecorderClient {
	static final long RECORD_TIME = 60000; // 1 minute

	// path of the wav file
	// File wavFile = new File("E:/Test/RecordAudio.wav");

	// format of audio file
	AudioFileFormat.Type fileType = AudioFileFormat.Type.WAVE;

	// the line from which audio data is captured
	TargetDataLine line;
	static ByteArrayOutputStream out;
	/**
	 * Defines an audio format
	 */
	AudioFormat getAudioFormat() {
		float sampleRate = 16000;
		int sampleSizeInBits = 8;
		int channels = 2;
		boolean signed = true;
		boolean bigEndian = true;
		AudioFormat format = new AudioFormat(sampleRate, sampleSizeInBits,
				channels, signed, bigEndian);
		return format;
	}

	/**
	 * Captures the sound and record into a WAV file
	 */
	void start() {
		try {
			AudioFormat format = getAudioFormat();
			DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);

			// checks if system supports the data line
			if (!AudioSystem.isLineSupported(info)) {
				System.out.println("Line not supported");
				System.exit(0);
			}
			line = (TargetDataLine) AudioSystem.getLine(info);
			line.open(format);
			line.start(); // start capturing

			System.out.println("Start capturing...");

			//AudioInputStream ais = new AudioInputStream(line);
			//int numBytesRead;
			System.out.println("Start recording...");
			int bufferSize = (int) format.getSampleRate()
					* format.getFrameSize();
			byte buffer[] = new byte[bufferSize];
			line.read(buffer, 0, buffer.length);
			out = new ByteArrayOutputStream();

			out.write(buffer, 0,buffer.length);
			// start recording
			// AudioSystem.write(ais, fileType, wavFile);

		} catch (LineUnavailableException ex) {
			ex.printStackTrace();
		}
		// } catch (IOException ioe) {
		// ioe.printStackTrace();
		// }
	}

	/**
	 * Closes the target data line to finish capturing and recording
	 */
	void finish() {
		line.stop();
		line.close();
		System.out.println("Finished");
	}

	/**
	 * Entry to run the program
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		Vertx vertx = VertxFactory.newVertx();
		vertx.createNetClient().connect(1234, "localhost",
				new AsyncResultHandler<NetSocket>() {
					public void handle(AsyncResult<NetSocket> asyncResult) {
						if (asyncResult.succeeded()) {
							NetSocket socket = asyncResult.result();
							socket.dataHandler(new Handler<Buffer>() {
								public void handle(Buffer buffer) {
									System.out.println("Net client receiving: "
											+ buffer);
								}
							});
							  //Now send some data
							final RecorderClient recorder = new RecorderClient();

							// creates a new thread that waits for a specified
							// of time before stopping
							Thread stopper = new Thread(new Runnable() {
								public void run() {
									try {
										Thread.sleep(RECORD_TIME);
									} catch (InterruptedException ex) {
										ex.printStackTrace();
									}
									recorder.finish();
								}
							});

							stopper.start();

							// start recording
							recorder.start();
							socket.write(new Buffer(out.toByteArray()));

						} else {
							asyncResult.cause().printStackTrace();
						}
					}
				});
		System.in.read();
	}

}
